//package com.tbs.generic.vansales.Activitys;
//
//import android.content.Context;
//
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.tbs.generic.vansales.Adapters.PriceItemAdapter;
//import com.tbs.generic.vansales.Model.PriceDO;
//import com.tbs.generic.vansales.Model.PriceItemDO;
//import com.tbs.generic.vansales.Model.PriceItemMainDO;
//import com.tbs.generic.vansales.R;
//import com.tbs.generic.vansales.Requests.PriceITEMRequest;
//
//import java.util.ArrayList;
//
//public class PriceItemsActivity extends BaseActivity {
//    private String userId, Code;
//    private RecyclerView recycleview;
//    private RelativeLayout llOrderHistory;
//    private TextView tvNoOrders;
//    ArrayList<PriceItemDO> priceItemDOS;
//    LinearLayout ll1;
//    ListView listView;
//
//
//    @Override
//    public void initialize() {
//        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.price_item_screen, null);
//        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        changeLocale();
//        initializeControls();
//        toolbar.setNavigationIcon(R.drawable.back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        if (getIntent().hasExtra("Code")) {
//            Code = getIntent().getExtras().getString("Code");
//        }
//        recycleview.setLayoutManager(new LinearLayoutManager(this));
//
//        listView = findViewById(R.id.lvItems);
//
//        PriceDO priceDO = new PriceDO();
//        showLoader();
//        PriceITEMRequest priceListRequest = new PriceITEMRequest(Code, PriceItemsActivity.this);
//        priceListRequest.setOnResultListener(new PriceITEMRequest.OnResultListener() {
//            @Override
//            public void onCompleted(boolean isError, PriceItemMainDO priceDOS) {
//                hideLoader();
//                priceDOS = priceDOS;
//                if (isError) {
//                    Toast.makeText(PriceItemsActivity.this, getString(R.string.failed_to_get_price_list), Toast.LENGTH_SHORT).show();
//                } else {
//                    priceItemDOS = priceDOS.priceItemDOS;
//
//                    if (priceItemDOS.size() > 0) {
//                        ll1.setVisibility(View.GONE);
//                        recycleview.setVisibility(View.VISIBLE);
//                        tvNoOrders.setVisibility(View.GONE);
//                        PriceItemAdapter driverAdapter = new PriceItemAdapter(PriceItemsActivity.this, priceItemDOS);
//                        recycleview.setAdapter(driverAdapter);
////                              showAppCompatAlert("Alert!","No Records", "OK", "", "FAILURE", false);
//                    } else {
//                        recycleview.setVisibility(View.GONE);
//                        tvNoOrders.setVisibility(View.VISIBLE);
//                        ll1.setVisibility(View.GONE);
//                    }
//
////                    CustomAdapter customAdapter = new CustomAdapter(PriceItemsActivity.this, priceDOS.priceItemDOS);
////                    // Set a adapter object to the listview
////                    listView.setAdapter(customAdapter);
//                }
//            }
//        });
//
//        priceListRequest.execute();
//        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);
//
//    }
//
//    class CustomAdapter extends BaseAdapter {
//        public Context context;
//        ArrayList<PriceItemDO> siteDetailsDos;
//
//
//        public CustomAdapter(Context context, ArrayList<PriceItemDO> customerDetailsDos) {
//
//            this.context = context;
//            this.siteDetailsDos = customerDetailsDos;
//        }
//
//        @Override
//        public int getCount() {
//            // return the all apps count
//            if (siteDetailsDos.size() > 0)
//                return siteDetailsDos.size();
//            else
//                return 0;
//        }
//
//        @Override
//        public Object getItem(int position) {
//            // TODO Auto-generated method stub
//            return null;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            // TODO Auto-generated method stub
//            return position;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            // Inflating installedapps_customlayout
//            View view = getLayoutInflater().inflate(R.layout.price_item_data, null);
//            if (siteDetailsDos.size() > 0) {
//                TextView addressLine = view.findViewById(R.id.tvPriceId);
//                TextView addressDescription = view.findViewById(R.id.tvPriceName);
//                TextView postalCode = view.findViewById(R.id.tvPriceValue);
//                addressLine.setText(getString(R.string.record) + " : " + siteDetailsDos.get(position).record);
//                addressDescription.setText(getString(R.string.start_Date) + " : " + siteDetailsDos.get(position).startDate);
//                postalCode.setText(getString(R.string.end_date) + " : " + siteDetailsDos.get(position).endDate);
//            } else {
//                showAlert(getString(R.string.no_records_found));
//            }
//
//
//            return view;
//        }
//
//    }
//
//    @Override
//    public void initializeControls() {
//        tvScreenTitle.setText(R.string.price_list);
//        recycleview = findViewById(R.id.recycleview);
//        ll1 = findViewById(R.id.ll1);
//
//        tvNoOrders = findViewById(R.id.tvNoOrders);
//
//    }
//
//}
