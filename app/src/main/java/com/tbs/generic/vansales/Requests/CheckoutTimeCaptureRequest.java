package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.Model.VehicleCheckInDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CheckoutTimeCaptureRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String userID, currentDate;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private int flaG;

    public CheckoutTimeCaptureRequest(String userId, String currentdate, Context mContext) {

        this.mContext = mContext;
        this.userID = userId;
        this.currentDate = currentdate;


    }


    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO createInvoiceDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String doc_number = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String openReading = preferenceUtils.getStringFromPreference(PreferenceUtils.OPENING_READING, "");
        String closeReading = preferenceUtils.getStringFromPreference(PreferenceUtils.CLOSING_READING, "");
        int reading2 = preferenceUtils.getIntFromPreference(PreferenceUtils.ODO_READ, 0);
        VehicleCheckInDo vehicleCheckInDo = StorageManager.getInstance(mContext).getVehicleCheckInData(mContext);

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_X10CUSRID", userID);
            jsonObject.put("I_XDATE", currentDate);
            jsonObject.put("I_XDECONARRDAT", vehicleCheckInDo.getCheckOutTimeCaptureArrivalDate());
            jsonObject.put("I_XDECONARRTIM", vehicleCheckInDo.getCheckOutTimeCaptureArrivalTime());
            jsonObject.put("I_XDESTRUNLODDAT", vehicleCheckInDo.getCheckOutTimeCaptureStartLoadingDate());
            jsonObject.put("I_XDESTRUNLODTIM", vehicleCheckInDo.getCheckOutTimeCaptureStartLoadingTime());
            jsonObject.put("I_XDEUNLODSTKDAT", vehicleCheckInDo.getCheckOutTimeCaptureStockLoadingDate());
            jsonObject.put("I_XDEUNLODSTKTIM", vehicleCheckInDo.getCheckOutTimeCaptureStockLoadingTime());
            jsonObject.put("I_XDEENDUNLODDAT", vehicleCheckInDo.getCheckOutTimeCaptureEndLoadingDate());
            jsonObject.put("I_XDEENDUNLODTIM", vehicleCheckInDo.getCheckOutTimeCaptureEndLoadingTime());
            jsonObject.put("I_XDEVEHINPDAT", vehicleCheckInDo.getCheckOutTimeCapturVehicleInspectionDate());
            jsonObject.put("I_XDEVEHINPTIM", vehicleCheckInDo.getCheckOutTimeCaptureVehicleInspectionTime());
            jsonObject.put("I_XDEGATINPDAT", vehicleCheckInDo.getCheckOutTimeCapturGateInspectionDate());
            jsonObject.put("I_XDEGATINPTIM", vehicleCheckInDo.getCheckOutTimeCaptureGateInspectionTime());
            jsonObject.put("I_XDECHKOUTDAT", vehicleCheckInDo.getCheckOutTimeCapturcheckOutDate());
            jsonObject.put("I_XDECHKOUTTIM", CalendarUtils.getTime());
            jsonObject.put("I_XCOGEOX", vehicleCheckInDo.getCheckoutLattitude());
            jsonObject.put("I_XCOGEOY", vehicleCheckInDo.getCheckoutLongitude());
            jsonObject.put("I_XTRANUM", doc_number);
            if (openReading.isEmpty()) {
                jsonObject.put("I_XSODORED", reading2);

            } else {
                jsonObject.put("I_XSODORED", openReading);

            }

            jsonObject.put("I_XFODORED", closeReading);
            jsonObject.put("I_XLOADBAY", preferenceUtils.getIntFromPreference(PreferenceUtils.LOAD_BAY,0));


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
//        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CHECKIN_STATUS, jsonObject);

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CHECKOUT_DATE_TIME, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        }
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

//        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }
}