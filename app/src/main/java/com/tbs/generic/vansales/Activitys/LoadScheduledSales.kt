package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.Util

//
class LoadScheduledSales : BaseActivity() {

    override fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.fragment_scheduled_sales, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.load_scheduledsales)
        val btnLoad = findViewById<Button>(R.id.btnLoad)

        btnLoad.setOnClickListener{
            Util.preventTwoClick(it)
            val intent = Intent(this@LoadScheduledSales, DashBoardActivity::class.java)
            startActivity(intent)
        }
    }
}