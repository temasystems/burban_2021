package com.tbs.generic.vansales.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.LoadStockMainDO
import com.tbs.generic.vansales.Model.NonScheduledProductMainDO
import com.tbs.generic.vansales.Model.ProductDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.PreferenceUtils

/**
 * Created by sandy on 2/15/2018.
 */

class LoadStockNonScheduledFragmment : Fragment() {
    lateinit var productDOS: List<ProductDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnCompleted: Button
    lateinit var loadStockMainDO: LoadStockMainDO
    lateinit var tvNoDataFound : TextView
    lateinit var loadStockDOs : ArrayList<LoadStockDO>
    lateinit var nonScheduledProductMainDO: NonScheduledProductMainDO
    var driverID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadStockDOs = arguments!!.getSerializable("LoadData") as ArrayList<LoadStockDO>
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.unsheduled_fragment, container, false)
        recycleview = view.findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = view.findViewById<View>(R.id.tvNoDataFound) as TextView
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        var preferenceUtils = (activity as BaseActivity).preferenceUtils
        val name                  = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_NAME, "")
        val date                  = preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE, "")
        driverID                          = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "QUA02")
        var routeId               = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")

        if (loadStockDOs!=null && loadStockDOs.size>0) {
            var loadStockAdapter = LoadStockAdapter(context, loadStockDOs, "")
            recycleview.adapter = loadStockAdapter
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else {
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return view
    }
}

    private operator fun Double.plus(load: String?): Any {
        return load!!
    }
