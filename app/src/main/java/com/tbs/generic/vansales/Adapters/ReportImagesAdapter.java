package com.tbs.generic.vansales.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tbs.generic.vansales.Model.ReportImagesDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.listeners.StringListner;

import java.util.List;

public class ReportImagesAdapter extends RecyclerView.Adapter<ReportImagesAdapter.MyViewHolder> {
    private LayoutInflater layoutInflater;
    private Context context;
    private List<ReportImagesDO> filesList;
    private int imageWd;
    private StringListner stringListner;
    private int selectedPos = 0;

    /**
     * Instantiates a new Files preview adapter.
     *
     * @param context       the context
     * @param filesList     the files list
     * @param stringListner the string listner
     */
    public ReportImagesAdapter(Context context, List<ReportImagesDO> filesList
            , StringListner stringListner) {
        this.stringListner = stringListner;
        layoutInflater = LayoutInflater.from(context);
        imageWd = (int) context.getResources().getDimension(R.dimen._40sdp);
        this.context = context;
        this.filesList = filesList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(layoutInflater.inflate(R.layout.files_preview_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final ReportImagesDO fileDetails = filesList.get(position);
        holder.ivRemove.setVisibility(View.GONE);
        byte[] imageByteArray = Base64.decode(fileDetails.image, Base64.DEFAULT);

        Glide.with(context)
                .load(imageByteArray)
                .centerCrop()
                .override(imageWd, imageWd)
                .into(holder.ivFilePreview);
//        holder.mainRel.setOnClickListener(v -> {
//            Intent intent = new Intent(context, PhotoDetailsActivity.class);
//            //intent.putExtra(Constants.IMAGE, fileDetails);
//            AppPrefs.selectedImage = fileDetails.image;
//            context.startActivity(intent);
//        });
    }


    /**
     * Sets position.
     *
     * @param position the position
     */
    public void setPosition(int position) {
        selectedPos = position;
    }

    @Override
    public int getItemCount() {
        return filesList.size();
    }

    /**
     * The type My view holder.
     */
    class MyViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Iv file preview.
         */
        ImageView ivFilePreview;
        /**
         * The Iv remove.
         */
        ImageView ivRemove;
        /**
         * The Tv file size.
         */
        TextView tvFileSize;
        /**
         * The Main rel.
         */
        RelativeLayout mainRel;

        /**
         * Instantiates a new My view holder.
         *
         * @param itemView the item view
         */
        MyViewHolder(View itemView) {
            super(itemView);

            ivFilePreview = itemView.findViewById(R.id.iv_file_preview);
            ivRemove = itemView.findViewById(R.id.iv_remove);
            tvFileSize = itemView.findViewById(R.id.tv_file_size);
            mainRel = itemView.findViewById(R.id.main_rel);

        }
    }
}
