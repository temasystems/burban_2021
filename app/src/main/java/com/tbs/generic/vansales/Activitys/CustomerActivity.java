package com.tbs.generic.vansales.Activitys;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tbs.generic.vansales.Adapters.CustomerAdapter;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.Requests.CustomerNewRequest;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class CustomerActivity extends BaseActivity {
    private String userId, orderCode;
    private PreferenceUtils preferenceUtils;
    private RecyclerView recycleview;
    private CustomerAdapter customerAdapter;
    private RelativeLayout llOrderHistory;
    private TextView tvNoOrders;
    private List<CustomerDo> customerDos;


    @Override
    public void initialize() {
        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.customer_screen, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        changeLocale();
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        preferenceUtils = new PreferenceUtils(CustomerActivity.this);
        recycleview.setLayoutManager(new LinearLayoutManager(CustomerActivity.this));
        if (Util.isNetworkAvailable(this)) {
            CustomerNewRequest driverListRequest = new CustomerNewRequest(CustomerActivity.this);
            driverListRequest.setOnResultListener(new CustomerNewRequest.OnResultListener() {
                @Override
                public void onCompleted(boolean isError, ArrayList<CustomerDo> customerDOs) {
                    customerDos = customerDOs;
                    if (isError) {
                        Toast.makeText(CustomerActivity.this, R.string.error_customer_list, Toast.LENGTH_SHORT).show();
                    } else {

                        customerAdapter = new CustomerAdapter(CustomerActivity.this, customerDos);
                        recycleview.setAdapter(customerAdapter);

                    }
                }
            });
            driverListRequest.execute();
        } else {
            showAppCompatAlert(getString(R.string.alert), getResources().getString(R.string.internet_connection), getString(R.string.ok), "", "",false);

        }



    }

    @Override
    public void initializeControls() {
        recycleview = findViewById(R.id.recycleview);
        tvNoOrders = findViewById(R.id.tvNoOrders);
        tvScreenTitle.setText(R.string.customer_list);
        ivSearch.setVisibility(View.VISIBLE);

    }

}
