package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class LoadStockDO implements Serializable {
    public String shipmentNumber = "";
    public String product = "";
    public String productDescription = "";
    public String stockUnit = "";
    public String weightUnit = "";
    public int quantity = 0;
    public double productWeight = 0.0;
    public double productVolume = 0.0;
    public String totalStockUnit = "";
    public String totalStockVolume = "";
    public int itemCount = 0;
    public int status;
    public int productGroupType = 0;
    public String productUnit = "";

    public String other = "";


}
