package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CaptureTypeDo;
import com.tbs.generic.vansales.Model.CaptureTypeMainDO;
import com.tbs.generic.vansales.Model.ReasonDO;
import com.tbs.generic.vansales.Model.ReasonMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

public class UserorDriverCostTypeRequest extends AsyncTask<String, Void, Boolean> {

    private CaptureTypeMainDO captureTypeMainDO;
    private CaptureTypeDo captureTypeDo;
    private Context mContext;
    private int type;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String customer;


    public UserorDriverCostTypeRequest(int type,Context mContext) {

        this.mContext = mContext;

this.type=type;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, CaptureTypeMainDO reasonMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_XFLG",type);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.DAMAGE_REASONS_LIST, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            captureTypeMainDO = new CaptureTypeMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        captureTypeMainDO.reasonDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        captureTypeDo = new CaptureTypeDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_XTEXTCOD")) {
                            captureTypeDo.typeId = Integer.parseInt(text);


                        }
                        else if (attribute.equalsIgnoreCase("O_XTEXT")) {
                            captureTypeDo.typetext = text;

                        }

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        captureTypeMainDO.reasonDOS.add(captureTypeDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if(xpp.getText().length()>0){

                        text = xpp.getText();
                    }else {
                        text="";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity)mContext).hideLoader();

      //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, captureTypeMainDO);
        }
    }
}