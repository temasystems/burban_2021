package com.tbs.generic.vansales.database.models

import com.google.gson.annotations.SerializedName

/**
 *Created by VenuAppasani on 12-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
data class SaveStockDBModel (
        @SerializedName("user_id") var user_id: String? = "",
        @SerializedName("item_id") var item_id: String? = "",
        @SerializedName("item_desc") var item_desc: String? = "",
        @SerializedName("item_quantity") var item_quantity: String? = "",
        @SerializedName("item_weight") var item_weight: String? = "",
        @SerializedName("item_mass") var item_mass: String? = "",
        @SerializedName("status") var status: String? = "",
        @SerializedName("other") var other: String? = "",
        var isSelected: Boolean? = false
)