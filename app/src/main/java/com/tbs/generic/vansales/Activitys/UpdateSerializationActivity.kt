package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.tbs.generic.vansales.Adapters.*
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.utils.*
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_equipserial_number_allocation.*
import kotlinx.android.synthetic.main.activity_serial_number_allocation.*
import kotlinx.android.synthetic.main.activity_serial_number_allocation.btn_save
import kotlinx.android.synthetic.main.activity_serial_number_allocation.etSearchh
import kotlinx.android.synthetic.main.activity_serial_number_allocation.ivClearSearchh
import kotlinx.android.synthetic.main.activity_serial_number_allocation.llSearchh
import kotlinx.android.synthetic.main.activity_serial_number_allocation.tvBookedQty
import kotlinx.android.synthetic.main.activity_serial_number_allocation.tv_product
import kotlinx.android.synthetic.main.configuration_layout.*
import kotlinx.android.synthetic.main.include_inspection_info.*
import kotlinx.android.synthetic.main.include_inspection_summary.*
import kotlinx.android.synthetic.main.purchase_stock_data.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.lang.Double
import java.util.*
import kotlin.collections.ArrayList


class UpdateSerializationActivity : BaseActivity() {

    lateinit var invoiceAdapter: InvoiceAdapter
    var recieptDos: ArrayList<ActiveDeliveryDO> = ArrayList()
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var serialLotDO: ActiveDeliveryDO = ActiveDeliveryDO()
    lateinit var view: LinearLayout
    lateinit var btnAddSerialLot: Button
    lateinit var btnConfirm: Button
    var selectDOS: ArrayList<SerialListDO>? = null
    lateinit var adapter: UpdateSerialSelectionAdapter

    var fromId = 0
    lateinit var serialDOS: ArrayList<SerialListDO>

    lateinit var activeDeliveryDO: ActiveDeliveryDO


    private var initAL: String? = null // keep starting AN
    private var BASE: Int = 0 // base of numberset is 62 so, addition = (anynumber %
    // BASE) and spare = (anynumber / 62)

    override fun onResume() {
        super.onResume()
//        selectInvoiceList()
    }


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.activity_serial_number_allocation, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()
    }

    override fun initializeControls() {
        tv_title.setText(resources.getString(R.string.serialallocation))
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.rcv_stock_selection)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        recycleview.layoutManager = linearLayoutManager
        this.initAL = initAL

        if (intent.hasExtra("PRODUCTDO")) {
            activeDeliveryDO = intent.extras!!.getSerializable("PRODUCTDO") as ActiveDeliveryDO
        }

        tv_product.setText(activeDeliveryDO.product + " - " + activeDeliveryDO.productDescription)

        tvBookedQty.setText("" + activeDeliveryDO.orderedQuantity)
        setData()
        btn_save.setOnClickListener {
            serialDOS= ArrayList()
            var count=0

            for (i in selectDOS!!.indices) {
                if (selectDOS!!.get(i).ischecked==true) {
                    count=count+1
                    serialDOS.add(selectDOS!!.get(i))

                }

            }
            if (adapter!=null&&count== activeDeliveryDO.orderedQuantity) {
                saveSerials(1)
            } else {
                serialDOS= ArrayList()

                showToast(resources.getString(R.string.qty_notmatching))
            }
        }
        btn_allocate.visibility=View.VISIBLE
        btn_allocate.setOnClickListener {
            saveSerials(2)

        }
        llSearchh.visibility = View.VISIBLE


        ivClearSearchh.setOnClickListener {
            Util.preventTwoClick(it)
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            etSearchh.requestFocus()
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
            llSearchh.visibility = View.VISIBLE

        }

        btn_scan.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(applicationContext, TestBarcodeActivity::class.java)

            startActivityForResult(intent, 11)
        }

//
        etSearchh.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if (etSearchh.text.toString().equals("", true)) {
                    if (selectDOS != null && selectDOS!!.size > 0) {
//                        for (i in selectDOS!!.indices) {
//                            selectDOS!!.get(i).ischecked=false
//                        }
                        adapter = UpdateSerialSelectionAdapter(activeDeliveryDO, this@UpdateSerializationActivity, selectDOS)
                        recycleview.adapter = adapter
                        tvNoDataFound.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    } else {
                        tvNoDataFound.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                } else if (etSearchh.text.toString().length > 1) {
                    filter(etSearchh.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun setData() {

        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = UpdateSerialListRequest(activeDeliveryDO, this@UpdateSerializationActivity)
            driverListRequest.setOnResultListener { isError, serialDOS ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                } else {
                    if (serialDOS != null && serialDOS.size > 0) {
                        selectDOS = serialDOS

                        adapter = UpdateSerialSelectionAdapter(activeDeliveryDO, this@UpdateSerializationActivity, serialDOS)
                        recycleview.adapter = adapter
                    } else {
                        tvNoDataFound.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }
    }

    private fun filter(filtered: String): ArrayList<SerialListDO> {
        val customerDOs = ArrayList<SerialListDO>()
        for (i in selectDOS!!.indices) {
            if (selectDOS!!.get(i).serialDO.contains(filtered, true)
                    || selectDOS!!.get(i).serialDO.contains(filtered, true)) {
                customerDOs.add(selectDOS!!.get(i))
            }
        }
        if (customerDOs.size > 0) {
            adapter.refreshAdapter(customerDOs)
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        } else {
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }

    override fun onBackPressed() {
        if (!svSearch.isIconified) {
            svSearch.isIconified = true
            return
        }
        super.onBackPressed()
    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            val siteListRequest = CreateLotRequest(recieptDos, this@UpdateSerializationActivity)
            siteListRequest.setOnResultListener { isError, approveDO, msg ->
                hideLoader()

                if (isError) {
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                    }
                } else {
                    if (approveDO.flag == 2) {
                        val intent = Intent()
                        intent.putExtra("RecieptDOS", recieptDos)
                        setResult(10, intent)
                        finish()
                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)
                    }

                }

            }
            siteListRequest.execute()

        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {

            }

    }

    private fun saveSerials(flag: Int) {

        if (Util.isNetworkAvailable(this)) {
            showLoader()
            val request = UpdateSerialsRequest(
                    this, flag,
                    serialDOS,activeDeliveryDO
            )
            request.setOnResultListener { isError, contarctMainDO ->
                hideLoader()
                if (isError) {
                    showToast(resources.getString(R.string.server_error));

                } else {
                    if (contarctMainDO != null) {
                        if (contarctMainDO.flag == 20) {
                            if (flag == 2) {
                                setData()
                            } else {
                                showToast(contarctMainDO.successFlag)

//                            val intent = Intent()
//                            intent.putExtra("NOTES", activeDeliveryDO.)
//                            setResult(Activity.RESULT_OK, intent)
                                finish()//                           modify(contarctMainDO.successFlag)
                            }

                        } else {
                            showToast(resources.getString(R.string.server_error));

                        }

                    } else {
                        showToast(resources.getString(R.string.server_error));

                    }
                }

            }
            request.execute()
        } else {
            showToast(resources.getString(R.string.internet_connection));
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        var stockDos = StorageManager.getInstance(this).getSerialList(this)

        if (requestCode == 11 && resultCode == 11) {
            serialDOS= ArrayList()
            var count=0

            for (i in selectDOS!!.indices) {
                if (selectDOS!!.get(i).ischecked==true) {
                    count=count+1
                    serialDOS.add(selectDOS!!.get(i))

                }

            }
            if (adapter!=null&&count< activeDeliveryDO.orderedQuantity) {
                var serial = data!!.getStringExtra("SERIAL")
                var stockItemDo = SerialListDO()
                stockItemDo.serialDO = serial
                stockDos.add(stockItemDo)
                var itemDo = StockItemDo()
                var boolean:Boolean=false
                var stockDos = StorageManager.getInstance(this).getSerialList(this)
                for (i in selectDOS!!.indices) {
                    if (selectDOS!!.get(i).serialDO.equals(serial)) {
                        boolean=true
                        selectDOS!!.get(i).ischecked=true
                        selectDOS!!.get(i).state=2

                        break
                    } else {
                        boolean=false
                    }

                }
                if(boolean==true){
                    adapter.refreshAdapter(selectDOS!!)

                }else{
                    showToast(getString(R.string.no_serial_found))
                }
            }else{
                showToast(getString(R.string.qty_notmatching))

            }


        }
        if (stockDos.size > 0) {
            tvNoDataFound.visibility = View.GONE
        }
    }
}