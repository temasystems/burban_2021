package com.tbs.generic.vansales.Activitys

import android.content.Intent
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tbs.generic.vansales.Adapters.LoanReturnAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.LoanReturnDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.SalesReturnListRequest
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util

//
class CaptureSalesReturnDetailsActivity : BaseActivity() {
    lateinit var loanReturnAdapter: LoanReturnAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    lateinit var tvOpeningQty:TextView
    lateinit var tvIssuedQty:TextView
    private var openingQty: String? = ""
    private var issuedQty: Int? = 0

    override fun initialize() {
      val  llCategories = layoutInflater.inflate(R.layout.capture_return_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvScreenTitle.text = getString(R.string.sales_return)
        if(intent.hasExtra("IssuedQty"))
            issuedQty = intent.extras?.getInt("IssuedQty", 0)
        initializeControls()

        tvIssuedQty.text = getString(R.string.issued_qty)+" : "+issuedQty
    }

    override fun initializeControls() {

        recycleview             = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        tvNoDataFound           = findViewById<TextView>(R.id.tvNoDataFound)
        tvOpeningQty            = findViewById<TextView>(R.id.tvOpeningQty)
        tvIssuedQty             = findViewById<TextView>(R.id.tvIssuedQty)
        val tvCustomerName = findViewById<View>(R.id.tvCustomerName) as TextView
        val btnConfirm      = findViewById<Button>(R.id.btnConfirm)
        val linearLayoutManager     = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recycleview.layoutManager = linearLayoutManager

        tvCustomerName.text = getString(R.string.customer)+" : "+preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER,"")
        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = SalesReturnListRequest(this@CaptureSalesReturnDetailsActivity)
            driverListRequest.setOnResultListener { isError, loanReturnMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    btnConfirm.visibility = View.GONE
                    showToast(resources.getString(R.string.server_error))
                } else {
                    openingQty = loanReturnMainDO.openingQty.toString()
                    val loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>()
                    var shipmentIds = ArrayList<String>()
                    if(loanReturnMainDO!=null && loanReturnMainDO.loanReturnDOS.size>0){
                        for (i in loanReturnMainDO.loanReturnDOS.indices){
                            if(!shipmentIds.contains(loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber)){
                                shipmentIds.add(loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber)
                            }
                        }
                    }

                    if(shipmentIds.size>0){
                        for (j in shipmentIds.indices){//4
                            var loanReturnDOS = ArrayList<LoanReturnDO>()
                            for (i in loanReturnMainDO.loanReturnDOS.indices) {//10
                                if (loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                                    loanReturnDOS.add(loanReturnMainDO.loanReturnDOS.get(i))
                                }
                            }
                            loanReturnDOsMap.put(shipmentIds.get(j), loanReturnDOS)
                        }
                    }

                    if(loanReturnDOsMap.size>0){
                        tvNoDataFound.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                        btnConfirm.visibility = View.VISIBLE
                        loanReturnAdapter = LoanReturnAdapter(this@CaptureSalesReturnDetailsActivity, loanReturnDOsMap,"Products")
                        recycleview.adapter = loanReturnAdapter
                        tvOpeningQty.text = getString(R.string.opening_qty)+" : "+loanReturnMainDO.openingQty
                    }else{
                        tvNoDataFound.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                        btnConfirm.visibility = View.GONE
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "",false)

        }


        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)


            if(loanReturnAdapter!=null){
                val loanReturnDos = loanReturnAdapter.selectedLoadStockDOs
                if(loanReturnDos!=null && !loanReturnDos.isEmpty()){
                    val intent = Intent(this@CaptureSalesReturnDetailsActivity, SelectedSalesReturnDetailsActivity::class.java)
                    intent.putExtra("Products", loanReturnDos)
                    intent.putExtra("openingQty", openingQty)
                    intent.putExtra("IssuedQty", issuedQty)
                    startActivityForResult(intent, 11)
                }
                else{
                    showToast(getString(R.string.please_select_items))
                }
            }
            else{
                showToast(getString(R.string.no_return_item_found))
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 11 && resultCode == 11){
            val intent = Intent()
            intent.putExtra("CapturedReturns", true)
           // AppConstants.CapturedReturns= true
            setResult(11, intent)
            finish()
        }


    }
}