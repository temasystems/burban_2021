package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.AddressDO;
import com.tbs.generic.vansales.Model.AddressMainDo;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class AddressListRequest extends AsyncTask<String, Void, Boolean> {

    private AddressMainDo addressMainDo;
    private AddressDO addressDO;
    private Context mContext;
    String customer;
    int type;


    public AddressListRequest(String code,int type, Context mContext) {

        this.mContext = mContext;
        this.customer = code;
        this.type = type;



    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, AddressMainDo bankMainDO);

    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_TYP",type);
            jsonObject.put("I_BPR",customer);


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext,ServiceURLS.runAction, WebServiceConstants.ADDRESSLIST, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            addressMainDo = new AddressMainDo();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {
                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        addressMainDo.addressDOS = new ArrayList<>();
                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        addressDO = new AddressDO();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_ADDCOD")) {
                            if(text.length()>0){
                                addressDO.addressCode = text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_ADDCODDES")) {
                            if(text.length()>0){

                                addressDO.addressDescription = text;
                            }

                        }

                    }
                    text="";

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        addressMainDo.addressDOS.add(addressDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if(xpp.getText().length()>0){

                        text = xpp.getText();
                    }else {
                        text="";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity)mContext).showLoader();
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity)mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, addressMainDo);
        }
    }
}