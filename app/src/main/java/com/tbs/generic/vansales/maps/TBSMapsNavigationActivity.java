//package com.tbs.generic.vansales.maps;
//
//import android.content.Intent;
//import android.location.Location;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.widget.Button;
//import android.widget.Toast;
//
//import com.mapbox.android.core.permissions.PermissionsListener;
//import com.mapbox.android.core.permissions.PermissionsManager;
//import com.mapbox.api.directions.v5.models.DirectionsResponse;
//import com.mapbox.api.directions.v5.models.DirectionsRoute;
//import com.mapbox.geojson.Point;
//import com.mapbox.mapboxsdk.Mapbox;
//import com.mapbox.mapboxsdk.annotations.Marker;
//import com.mapbox.mapboxsdk.geometry.LatLng;
//import com.mapbox.mapboxsdk.location.LocationComponent;
//import com.mapbox.mapboxsdk.location.modes.CameraMode;
//import com.mapbox.mapboxsdk.maps.MapView;
//import com.mapbox.mapboxsdk.maps.MapboxMap;
//import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
//import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
//import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
//import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
//import com.tbs.generic.vansales.Model.CustomerDo;
//import com.tbs.generic.vansales.R;
//import com.tbs.generic.vansales.database.StorageManager;
//import com.tbs.generic.vansales.utils.PreferenceUtils;
//
//
//import java.util.List;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//// classes needed to initialize map
//// classes needed to add the location component
//// classes needed to add a marker
//// classes to calculate a route
//// classes needed to launch navigation UI
//
///**
// * Created by VenuAppasani on 20-12-2018.
// * Copyright (C) 2018 TBS - All Rights Reserved
// */
//public class TBSMapsNavigationActivity extends AppCompatActivity implements OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener {
//    private MapView mapView;
//    // variables for adding location layer
//    private MapboxMap mapboxMap;
//    private PermissionsManager permissionsManager;
//    private Location originLocation;
//    // variables for adding a marker
//    private Marker destinationMarker;
//    private LatLng originCoord;
//    private LatLng destinationCoord;
//    // variables for calculating and drawing a route
//    private Point originPosition;
//    private Point destinationPosition;
//    private DirectionsRoute currentRoute;
//    private static final String TAG = "DirectionsActivity";
//    private NavigationMapRoute navigationMapRoute;
//    private Button button,start;
//    /* private double latitude = 17.437232;
//     private double longitude = 78.395185;
//
//     */
//    private double latitude ;
//    private double longitude;
//
//    private double destLatitude;
//    private double destLongitutde;
//    private PreferenceUtils preferenceUtils;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(this, getString(R.string.access_token_map));
//        setContentView(R.layout.activity_tbs_maps_navigation);
//        getIntentValues();
//        mapView = findViewById(R.id.mapView);
//        mapView.onCreate(savedInstanceState);
//        mapView.getMapAsync(this);
//    }
//
//    private void getIntentValues() {
//        preferenceUtils = new PreferenceUtils(this);
//        destLatitude = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE,0.0);
//        destLongitutde = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE,0.0);
//        //TODO: Remmove hard coded values before push to production
////        destLatitude = 17.490302;
////        destLongitutde = 78.392558;
//    }
//
//    @Override
//    public void onMapReady(MapboxMap mapboxMap) {
//        this.mapboxMap = mapboxMap;
//        enableLocationComponent();
//        //Pass appropriate lat and long values to get selected sequence route.
//        originCoord = new LatLng(latitude, longitude);
//        destinationCoord = new LatLng(destLatitude, destLongitutde);
//        mapboxMap.addOnMapClickListener(this);
//        button = findViewById(R.id.startButton);
//        start = findViewById(R.id.aboutTBSBTN);
//        start.setMaxLines(2);
//        start.setSingleLine(false);
//
//        String shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");
//        if (shipmentType.equals(getResources().getString(R.string.checkin_scheduled))) {
//            ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this);
//           String shipmentId = activeDeliverySavedDo.shipmentNumber;
//            start.setText(""+activeDeliverySavedDo.customerDescription + " \n "+activeDeliverySavedDo.city);
//        } else {
//            CustomerDo customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this);
//            start.setText(""+customerDo.customerName + " \n "+customerDo.city);
//
//        }
//
//
//        button.setOnClickListener(v -> {
//           /* boolean simulateRoute = false;
//            NavigationLauncherOptions options = NavigationLauncherOptions.builder()
//                    .directionsRoute(currentRoute)
//                    .shouldSimulateRoute(simulateRoute)
//                    .build();
//            // Call this method with Context from within an Activity
//            NavigationLauncher.startNavigation(TBSMapsNavigationActivity.this, options);*/
//            String latitude = "17.490302";
//            String longitude = "78.395124";
//            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + destLatitude + "," + destLongitutde);
//            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//            mapIntent.setPackage("com.google.android.apps.maps");
//
//            try{
//                if (mapIntent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(mapIntent);
//                }
//            }catch (NullPointerException e){
//                Log.e(TAG, "onClick: NullPointerException: Couldn't open map." + e.getMessage() );
//                Toast.makeText(TBSMapsNavigationActivity.this, "Couldn't open map", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        destinationPosition = Point.fromLngLat(destinationCoord.getLongitude(), destinationCoord.getLatitude());
//        originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
//        getRoute(originPosition, destinationPosition);
//        button.setEnabled(true);
//        button.setBackgroundResource(R.color.blue);
//    }
//
//    @Override
//    public void onMapClick(@NonNull LatLng point){
//       /* if (destinationMarker != null) {
//            mapboxMap.removeMarker(destinationMarker);
//        }
//        destinationCoord = point;
//        destinationMarker = mapboxMap.addMarker(new MarkerOptions()
//                .position(destinationCoord)
//        );
//        destinationPosition = Point.fromLngLat(destinationCoord.getLongitude(), destinationCoord.getLatitude());
//        originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
//        getRoute(originPosition, destinationPosition);
//        button.setEnabled(true);
//        button.setBackgroundResource(R.color.blue);*/
//    }
//
//    private void getRoute(Point origin, Point destination) {
//        NavigationRoute.builder(this)
//                .accessToken(Mapbox.getAccessToken())
//                .origin(origin)
//                .destination(destination)
//                .build()
//                .getRoute(new Callback<DirectionsResponse>() {
//                    @Override
//                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
//                        // You can get the generic HTTP info about the response
//                        Log.d(TAG, "Response code: " + response.code());
//                        if (response.body() == null) {
//                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
//                            return;
//                        } else if (response.body().routes().size() < 1) {
//                            Log.e(TAG, "No routes found");
//                            return;
//                        }
//
//                        try {
//                            currentRoute = response.body().routes().get(0);
//
//                            // Draw the route on the map
//                            if (navigationMapRoute != null) {
//                                navigationMapRoute.removeRoute();
//                            } else {
//                                navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
//                            }
//                            navigationMapRoute.addRoute(currentRoute);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
//                        Log.e(TAG, "Error: " + throwable.getMessage());
//                    }
//                });
//    }
//    @SuppressWarnings( {"MissingPermission"})
//    private void enableLocationComponent() {
//        // Check if permissions are enabled and if not request
//        if (PermissionsManager.areLocationPermissionsGranted(this)) {
//            // Activate the MapboxMap LocationComponent to show user location
//            // Adding in LocationComponentOptions is also an optional parameter
//            LocationComponent locationComponent = mapboxMap.getLocationComponent();
//            locationComponent.activateLocationComponent(this);
//            locationComponent.setLocationComponentEnabled(true);
//            // Set the component's camera mode
//            locationComponent.setCameraMode(CameraMode.TRACKING);
//            originLocation = locationComponent.getLastKnownLocation();
//            // latitude = originLocation.getLatitude();
//            //longitude = originLocation.getLongitude();
//           // latitude = 17.437232;
//            //longitude = 78.395185;
//            latitude =  17.518802;
//            longitude =  78.399274;
//        } else {
//            permissionsManager = new PermissionsManager(this);
//            permissionsManager.requestLocationPermissions(this);
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    @Override
//    public void onExplanationNeeded(List<String> permissionsToExplain) {
//        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onPermissionResult(boolean granted) {
//        if (granted) {
//            enableLocationComponent();
//        } else {
//            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
//            finish();
//        }
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        mapView.onStart();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mapView.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        mapView.onPause();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        mapView.onStop();
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        mapView.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mapView.onDestroy();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mapView.onLowMemory();
//    }
//}
//
