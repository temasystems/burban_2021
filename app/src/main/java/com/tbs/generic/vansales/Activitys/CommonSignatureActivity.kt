package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Environment
import android.os.Looper
import android.text.TextUtils
import androidx.core.app.ActivityCompat
import android.util.AttributeSet
import android.util.Base64
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.PermissionRequest
import android.widget.*
import androidx.core.widget.NestedScrollView
import com.bumptech.glide.Glide
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.models.Image
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.generic.vansales.Adapters.FilesPreviewAdapter
import com.tbs.generic.vansales.Adapters.ScheduledProductsAdapter
import com.tbs.generic.vansales.Model.CustomerManagementDO
import com.tbs.generic.vansales.Model.FileDetails
import com.tbs.generic.vansales.Model.PodDo
import com.tbs.generic.vansales.Model.ValidateDeliveryDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest
import com.tbs.generic.vansales.Requests.CylinderIssueRequest
import com.tbs.generic.vansales.Requests.ValidateDeliveryRequest
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.pdfs.BulkDeliveryNotePDF
import com.tbs.generic.vansales.pdfs.CYLDeliveryNotePDF
import com.tbs.generic.vansales.pdfs.CylinderIssRecPdfBuilder
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.activity_digital_signature.*
import kotlinx.android.synthetic.main.activity_digital_signature.nested_scroll
import kotlinx.android.synthetic.main.activity_signature_common.*
import kotlinx.android.synthetic.main.activity_signature_common.imv_signature
import kotlinx.android.synthetic.main.edit_document.*
import kotlinx.android.synthetic.main.include_image_caputure.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.DateFormat
import java.util.*

/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class CommonSignatureActivity : BaseActivity(), View.OnClickListener {
    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var file: File
    lateinit var mContent: LinearLayout
    lateinit var view: View
    lateinit var mSignature: signature
    var bitmap: Bitmap? = null
    lateinit var savedPath: String
    lateinit var encodedImage: String
    var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/UserSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/"signature"
    var StoredPath = DIRECTORY + pic_name + ".png"
    lateinit var podDo:PodDo
    var sign=""

    private var fileDetailsList: ArrayList<FileDetails>? = null

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.activity_signature_common, null) as NestedScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        disableMenuWithBackButton()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(9907, intent)
            finish()
//            showToast(getString(R.string.please_save_Sig))
        }


    }

    override fun initializeControls() {
        mContent = findViewById<View>(R.id.canvasLayout) as LinearLayout
        mSignature = signature(applicationContext, null)
        mSignature.setBackgroundColor(Color.WHITE)
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = findViewById<View>(R.id.clear) as Button
        mGetSign = findViewById<View>(R.id.getsign) as Button
        tvScreenTitle.setText(getString(R.string.confirm_sign))
        view = mContent
        mGetSign.setOnClickListener(this@CommonSignatureActivity)
        mClear.setOnClickListener(this@CommonSignatureActivity)


        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }
         podDo = StorageManager.getInstance(this).getDepartureData(this);


        if (intent.hasExtra("LD")) {
            var ld = intent.extras?.getInt("LD")!!
            if(ld==1){
                if (podDo.loanDeliveryRating!!.isNotEmpty()) {
                    ratingBar.rating = podDo.loanDeliveryRating!!.toFloat()
                }
                if(!podDo.loanDeliveryname.isNullOrEmpty()){
                    etName.setText(podDo.loanDeliveryname)
                }
                if(!podDo.loanDeliverysignature!!.isNullOrEmpty()){
                    sign= podDo.loanDeliverysignature!!

                }

            }


        }
        if (intent.hasExtra("LR")) {
            var ld = intent.extras?.getInt("LR")!!
            if(ld==1){
                if (podDo.loanReturnRating!!.isNotEmpty()) {
                    ratingBar.rating = podDo.loanReturnRating!!.toFloat()
                }
                if(!podDo.loanReturnname.isNullOrEmpty()){
                    etName.setText(podDo.loanReturnname)
                }
                if(!podDo.loanReturnsignature!!.isNullOrEmpty()){
                    sign= podDo.loanReturnsignature!!

                }
            }

        }
        if (intent.hasExtra("SD")) {
            var ld = intent.extras?.getInt("SD")!!
            if(ld==1){
                if (podDo.departureRating!!.isNotEmpty()) {
                    ratingBar.rating = podDo.departureRating!!.toFloat()
                }
                if(!podDo.name.isNullOrEmpty()){
                    etName.setText(podDo.name)
                }
                if(!podDo.signature!!.isNullOrEmpty()){
                    sign= podDo.signature!!

                }
            }

        }
        if (intent.hasExtra("RD")) {
            var ld = intent.extras?.getInt("RD")!!
            if(ld==1){
                if (podDo.departureRating!!.isNotEmpty()) {
                    ratingBar.rating = podDo.departureRating!!.toFloat()
                }
                if(!podDo.name.isNullOrEmpty()){
                    etName.setText(podDo.name)
                }
                if(!podDo.signature!!.isNullOrEmpty()){
                    sign= podDo.signature!!

                }
            }

        }
        if (!TextUtils.isEmpty(sign)) {
            mContent.visibility = View.GONE
            imv_signature.visibility = View.VISIBLE

            val imageByteArray: ByteArray = Base64.decode(sign, Base64.DEFAULT)
            Log.d("file_type", "base64")
            var imageWd = resources.getDimension(R.dimen._40sdp).toInt()
            Glide.with(this)
                    .load(imageByteArray)
                    .centerCrop()
                    .override(imageWd, imageWd)
                    .into(imv_signature)
        } else {
            mContent.visibility = View.VISIBLE
            imv_signature.visibility = View.GONE
        }

    }


    override fun onClick(v: View?) {
        Util.preventTwoClick(v)
        // TODO Auto-generated method stub
        if (v === mClear) {
            mSignature.clear()
        } else if (v === mGetSign) {
              if(etName.text.toString().isNotEmpty()){
                  if (Build.VERSION.SDK_INT >= 23) {
                      if (isStoragePermissionGranted()) {
                          view.setDrawingCacheEnabled(true)
                          mSignature.save(view, StoredPath)

                      }
                  } else {
                      view.setDrawingCacheEnabled(true)
                      mSignature.save(view, StoredPath)

                  }

              }else{
                  showToast(getString(R.string.plz_do_name))
              }


        }
    }

    override fun onButtonYesClick(from: String) {
        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
//            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()
        } else {
            Toast.makeText(this, getString(R.string.the_app_not_allowed), Toast.LENGTH_LONG).show()
        }
    }

    inner class signature(context: Context, attrs: AttributeSet?) : View(context, attrs) {
        private val paint = Paint()
        private val path = Path()

        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()

        init {
            paint.setAntiAlias(true)
            paint.setColor(Color.BLACK)
            paint.setStyle(Paint.Style.STROKE)
            paint.setStrokeJoin(Paint.Join.ROUND)
            paint.setStrokeWidth(STROKE_WIDTH)
        }

        @SuppressLint("WrongThread")
        fun save(v: View, StoredPath: String) {
            Log.v("log_tag", "Width: " + v.getWidth())
            Log.v("log_tag", "Height: " + v.getHeight())

            try {
                if (bitmap == null&&mContent.width>0&&mContent.height>0) {
                    bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
                    val canvas = bitmap?.let { Canvas(it) }
                    val mFileOutStream = FileOutputStream(StoredPath)
                    v.draw(canvas)
                    mFileOutStream.flush()
                    mFileOutStream.close()
                    val bos = ByteArrayOutputStream();
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, bos);
                    val bitmapdata = bos.toByteArray();
                    Log.e("Signature Size : ", "Size : " + bitmapdata)
                    encodedImage = Base64.encodeToString(bitmapdata, Base64.DEFAULT)

                    if (!path.isEmpty||sign!!.isNotEmpty()) {
                        val intent = Intent()
                        if(path.isEmpty){
                            intent.putExtra("Signature", sign)

                        }else{
                            intent.putExtra("Signature", encodedImage)

                        }
                        intent.putExtra("Rating", ratingBar.rating.toString())
                        intent.putExtra("Name", etName.text.toString())

                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    } else {
                        showToast(context.getString(R.string.please_do_sig))
                    }

                }
                else{
                    if (!path.isEmpty||sign!!.isNotEmpty()) {
                        val intent = Intent()
                        if(path.isEmpty){
                            intent.putExtra("Signature", sign)

                        }else{
                            intent.putExtra("Signature", encodedImage)

                        }
                        intent.putExtra("Rating", ratingBar.rating.toString())
                        intent.putExtra("Name", etName.text.toString())

                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    } else {
                        showToast(context.getString(R.string.please_do_sig))
                    }
                }


            } catch (e: Exception) {
                Log.v("log_tag", e.toString())
            }

        }

        fun clear() {
            path.reset()
            invalidate()
            imv_signature.visibility = View.GONE
            mContent.visibility = View.VISIBLE
        }

        fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                      filter: Boolean): Bitmap {
            val ratio = Math.min(
                    maxImageSize.toFloat() / realImage.getWidth(),
                    maxImageSize.toFloat() / realImage.getHeight())
            val width = Math.round(ratio.toFloat() * realImage.getWidth())
            val height = Math.round(ratio.toFloat() * realImage.getHeight())
            val newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter)
            return newBitmap
        }

        protected override fun onDraw(canvas: Canvas) {
            nested_scroll.requestDisallowInterceptTouchEvent(true);
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

    }

    companion object {
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }


    override fun onBackPressed() {
//        showToast("Please save signature")
        val intent = Intent()
        setResult(9907, intent)
        finish()
    }


}