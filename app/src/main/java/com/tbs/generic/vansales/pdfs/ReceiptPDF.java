package com.tbs.generic.vansales.pdfs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.zxing.WriterException;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.PaymentPdfDO;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.collector.CollectorPreviewActivity;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.pdfs.utils.PDFConstants;
import com.tbs.generic.vansales.pdfs.utils.PDFOperations;
import com.tbs.generic.vansales.pdfs.utils.PreviewActivity;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.NumberToWord;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/*
 * Created by developer on 27/1/19.
 */
public class ReceiptPDF {

    private static Activity context;
    private PaymentPdfDO paymentPdfDO;
    private PdfPTable parentTable;
    private Font normalFont;
    private Font boldFont;
    private Document document;
    private Font underLineFont;
    private String typE;

    public ReceiptPDF(Activity context) {
        ReceiptPDF.context = context;
    }

    public void createReceiptPDF(PaymentPdfDO paymentPdfDO, String type) {
        this.paymentPdfDO = paymentPdfDO;
        Log.d("paymentPdfDO", paymentPdfDO + "");
        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
        if (type.equalsIgnoreCase("Preview")) {
            normalFont = boldFont;
        }
        typE = type;
        underLineFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.UNDERLINE, BaseColor.BLACK);
        new CreateReceiptPDF().execute();

    }

    public void preparePDF(PaymentPdfDO paymentPdfDO, String type) {
        this.paymentPdfDO = paymentPdfDO;
        Log.d("paymentPdfDO", paymentPdfDO + "");
        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
        if (type.equalsIgnoreCase("Preview")) {
            normalFont = boldFont;
        }
        underLineFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.UNDERLINE, BaseColor.BLACK);
        new CreatePDF().execute();

    }

    public void prepareCollectorPDF(PaymentPdfDO paymentPdfDO, String type) {
        this.paymentPdfDO = paymentPdfDO;
        Log.d("paymentPdfDO", paymentPdfDO + "");
        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
        if (type.equalsIgnoreCase("Collector")) {
            normalFont = boldFont;
        }
        typE = type;

        underLineFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.UNDERLINE, BaseColor.BLACK);
        new CollectorCreatePDF().execute();

    }

    private class CreateReceiptPDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(context.getString(R.string.prep_reciept));
//            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            progressDialog.cancel();
            sendPDfewDoc();
        }
    }

    private class CreatePDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(context.getString(R.string.prep_reciept));
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.cancel();
            Intent intent = new Intent(context.getApplicationContext(), PreviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("KEY", PDFConstants.RECEIPT_PDF_NAME);
            context.startActivity(intent);
        }
    }

    private class CollectorCreatePDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(context.getString(R.string.prep_reciept));
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.cancel();
            Intent intent = new Intent(context.getApplicationContext(), CollectorPreviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("KEY", PDFConstants.RECEIPT_PDF_NAME);
            intent.putExtra("COLLECTOR", typE);
            context.startActivityForResult(intent,1);
        }
    }

    //Pre pare pdf
    private void preparePDF() {
        parentTable = new PdfPTable(1);
        parentTable.setWidthPercentage(100);
        initFile();
        // addLogo();
        addHeaderLogo();

        addHeaderLabel();
        addCustomerDetailsToPdf();
        addSumofAEDInfo();
        addPaymentsRowToPdf();
        if (!TextUtils.isEmpty(paymentPdfDO.chequeNumber)) {
            addChequeRowToPdf();

        }
        addInvoiceNoRowToPdf();
        parentTable.addCell(midleTable);
        try {
            document.add(parentTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        addRemarksRowToPdf();
//        addSignature();

        //addFooterToPdf();
        addQRCode();
        document.close();
    }

    private void initFile() {
        File file = new File(Util.getAppPath(context) + "TbsReceipt.pdf");
        if (file.exists())
            file.delete();

        try {
            document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A1);
            document.addCreationDate();
            document.addAuthor("TBS");
            document.addCreator("Venu Appasani");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addHeaderLogo() {

        try {
            document.add(PDFOperations.getInstance().
                    getHeadrLogoIOSCertificateLogo(context, document, paymentPdfDO.companyCode));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void addLogo() {
        try {
            InputStream ims = context.getAssets().open("brogas_logo.png");
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
            image.setAlignment(Element.ALIGN_CENTER);
            document.add(image);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addHeaderLabel() {
        try {
            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
            Paragraph headerLabel = new Paragraph(paymentPdfDO.company, headerLabelFont);
            headerLabel.setAlignment(Element.ALIGN_CENTER);
            headerLabel.setPaddingTop(10);
            Paragraph headerLabel1 = new Paragraph(context.getString(R.string.reciept_voucher), headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_CENTER);
            document.add(headerLabel);
            document.add(headerLabel1);
            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCustomerDetailsToPdf() {
        try {
            float[] columnWidths = {2.5f, 0.3f, 5.5f, 4f, 0.3f, 2.5f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellOne1 = new PdfPCell();
            cellOne1.setBorder(Rectangle.NO_BORDER);

            PdfPCell cellTwo = new PdfPCell();
            cellTwo.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree = new PdfPCell();
            cellThree.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree1 = new PdfPCell();
            cellThree1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellFour = new PdfPCell();
            cellFour.setBorder(Rectangle.NO_BORDER);


            cellOne.addElement(new Phrase(context.getString(R.string.reciept_no), boldFont));
            cellOne.addElement(new Phrase(context.getString(R.string.recieved_from), boldFont));
            cellOne.addElement(new Phrase(context.getString(R.string.address), boldFont));

            cellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));


            cellTwo.addElement(new Phrase(paymentPdfDO.paymentNumber));
            cellTwo.addElement(new Phrase(paymentPdfDO.customerDescription, normalFont));
            cellTwo.addElement(new Phrase(getAddress(), normalFont));


            cellThree.addElement(new Phrase(context.getString(R.string.date_pdf), boldFont));
            cellThree.addElement(new Phrase(context.getString(R.string.time_pdf), boldFont));
            cellThree.addElement(new Phrase(context.getString(R.string.user_id), boldFont));
            cellThree.addElement(new Phrase(context.getString(R.string.user_name), boldFont));

            cellThree1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellThree1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellThree1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellThree1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            if(paymentPdfDO.createdDate.length()>0){
                String dMonth = paymentPdfDO.createdDate.substring(4, 6);
                String dyear = paymentPdfDO.createdDate.substring(0, 4);
                String dDate = paymentPdfDO.createdDate.substring(Math.max(paymentPdfDO.createdDate.length() - 2, 0));

                cellFour.addElement(new Phrase(dDate + "-" + dMonth + "-" + dyear, normalFont));
            }else {
                cellFour.addElement(new Phrase(CalendarUtils.getPDFDate(context), normalFont));

            }

            cellFour.addElement(new Phrase(paymentPdfDO.createdTime, normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, ""), normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""), normalFont));
            cellFour.addElement(new Phrase(paymentPdfDO.createUserID, normalFont));
            cellFour.addElement(new Phrase(paymentPdfDO.createUserName, normalFont));
            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);

            parentTable.addCell(headerTable);
//            document.add(headerTable);


        } catch (Exception e) {
            Log.e("" + this.getClass().getName(), "Exce : " + e.getMessage());
        }

    }

    private PdfPCell midleTable = new PdfPCell();

    private void addSumofAEDInfo() {
        try {

            //Adding space
            PdfPTable emtyTable = new PdfPTable(1);
            PdfPCell emptyCell = new PdfPCell();
            emptyCell.setBorder(Rectangle.NO_BORDER);
            emptyCell.setMinimumHeight(5);
            emtyTable.addCell(emptyCell);
            midleTable.addElement(emtyTable);

            float[] productColumnsWidth = {2.35f, 0.3f, 8, 2.5f};
            PdfPTable sumofAEDInfoTable = new PdfPTable(productColumnsWidth);
            sumofAEDInfoTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellOne1 = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            cellOne1.setBorder(Rectangle.NO_BORDER);
            cellTwo.setBorder(Rectangle.NO_BORDER);
            cellThree.setBorder(Rectangle.NO_BORDER);

            Paragraph pOne = new Paragraph(context.getString(R.string.sum_of) + paymentPdfDO.currency, boldFont);
            pOne.setAlignment(Element.ALIGN_LEFT);
            cellOne.addElement(pOne);

            cellOne1.addElement(new Paragraph(context.getString(R.string.colon), boldFont));

            int afterDcimalVal = PDFOperations.getInstance().anyMethod(paymentPdfDO.amount);

            Paragraph pTwo = new Paragraph(getAmountInLetters(paymentPdfDO.amount) + context.getString(R.string.and) + getAmountInLetters(afterDcimalVal) + context.getString(R.string.cents), normalFont);
            pTwo.setAlignment(Element.ALIGN_LEFT);
            cellTwo.addElement(pTwo);

            PdfPTable pdfPTable = new PdfPTable(1);
            pdfPTable.setWidthPercentage(100);
            cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
            Paragraph pThree = new Paragraph(paymentPdfDO.currency + " " + paymentPdfDO.amount, boldFont);
            pThree.setAlignment(Element.ALIGN_CENTER);
            cellThree.addElement(pThree);
            pdfPTable.addCell(cellThree);

            sumofAEDInfoTable.addCell(cellOne);
            sumofAEDInfoTable.addCell(cellOne1);
            sumofAEDInfoTable.addCell(cellTwo);
            sumofAEDInfoTable.addCell(pdfPTable);

            midleTable.addElement(sumofAEDInfoTable);
//            document.add(sumofAEDInfoTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private String getAmountInLetters(double amount) {
//
//
//        double doubleNumber = amount;
//        String doubleAsString = String.valueOf(doubleNumber);
//        int indexOfDecimal = doubleAsString.indexOf(".");
//        System.out.println("Double Number: " + doubleNumber);
//        System.out.println("Integer Part: " + doubleAsString.substring(0, indexOfDecimal));
//        System.out.println("Decimal Part: " + doubleAsString.substring(indexOfDecimal));
//
//        double value = amount - Math.floor(amount);
//        String dString = Double.toString(value);
//        String secondValue = dString.split("\\.")[1];
//        //Spit double here
//        String finalReseult = "";
//
//        double v2= Double.parseDouble(secondValue);
//        finalReseult = finalReseult + new NumberToWord().convert((int) amount);
//        if (v2 > 0) {
//            finalReseult = finalReseult + " and " + new NumberToWord().convert((int) v2)+" Fills";
//        }
//        //Check now Anusha
//        String result = finalReseult.substring(0, 1).toUpperCase() + finalReseult.substring(1);
//        return result;
//        //return NumberToWords.getNumberToWords().getTextFromNumbers(amount);
//    }

    private String getAmountInLetters(double amount) {
        return new NumberToWord().convert((int) amount);
        //return NumberToWords.getNumberToWords().getTextFromNumbers(amount);
    }

    private void addPaymentsRowToPdf() {

        try {
            float[] productColumnsWidth = {1.61f, 0.3f, 1.5f, 1.5f, 0.3f, 4};
            PdfPTable paymentsTable = new PdfPTable(productColumnsWidth);
            paymentsTable.setWidthPercentage(100);

            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellOne1 = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            PdfPCell cellThree1 = new PdfPCell();
            PdfPCell cellFour = new PdfPCell();

            cellOne.setBorder(Rectangle.NO_BORDER);
            cellOne1.setBorder(Rectangle.NO_BORDER);
            cellTwo.setBorder(Rectangle.NO_BORDER);
            cellThree.setBorder(Rectangle.NO_BORDER);
            cellThree1.setBorder(Rectangle.NO_BORDER);
            cellFour.setBorder(Rectangle.NO_BORDER);

            cellOne.setHorizontalAlignment(Element.ALIGN_LEFT);
            Paragraph prOne = new Paragraph(context.getString(R.string.pay_method), boldFont);
            cellOne.addElement(prOne);

            cellOne1.addElement(new Paragraph(context.getString(R.string.colon), boldFont));

            cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
            Phrase phrase;
            if (!TextUtils.isEmpty(paymentPdfDO.chequeNumber)) {
                phrase = new Phrase(context.getString(R.string.by_cheq), normalFont);
            } else {
                phrase = new Phrase(context.getString(R.string.by_cash), normalFont);
            }
            cellTwo.addElement(phrase);


            cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
            if (!TextUtils.isEmpty(paymentPdfDO.chequeNumber)) {
                Paragraph prThree = new Paragraph(context.getString(R.string.bank), boldFont);
                cellThree.addElement(prThree);
                cellThree1.addElement(new Paragraph(context.getString(R.string.colon), boldFont));
                cellFour.setHorizontalAlignment(Element.ALIGN_RIGHT);
                Paragraph prFour = new Paragraph("" + paymentPdfDO.bank, normalFont);
                cellFour.addElement(prFour);
            }


            paymentsTable.addCell(cellOne);
            paymentsTable.addCell(cellOne1);
            paymentsTable.addCell(cellTwo);

            paymentsTable.addCell(cellThree);
            paymentsTable.addCell(cellThree1);
            paymentsTable.addCell(cellFour);

            midleTable.addElement(paymentsTable);
//            document.add(paymentsTable);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addChequeRowToPdf() {

        try {
            float[] productColumnsWidth = {1.61f, 0.3f, 1.5f, 1.5f, 0.3f, 4};
            PdfPTable paymentsTable = new PdfPTable(productColumnsWidth);
            paymentsTable.setWidthPercentage(100);

            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellOne1 = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            PdfPCell cellThree1 = new PdfPCell();
            PdfPCell cellFour = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            cellOne1.setBorder(Rectangle.NO_BORDER);
            cellTwo.setBorder(Rectangle.NO_BORDER);
            cellThree.setBorder(Rectangle.NO_BORDER);
            cellThree1.setBorder(Rectangle.NO_BORDER);
            cellFour.setBorder(Rectangle.NO_BORDER);

            cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
            Paragraph prOne = new Paragraph(context.getString(R.string.cheque_no), boldFont);
            cellOne.addElement(prOne);
            cellOne1.addElement(new Paragraph(context.getString(R.string.colon), boldFont));

            cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
            Phrase phrase = new Phrase("" + paymentPdfDO.chequeNumber, normalFont);
            cellTwo.addElement(phrase);

            cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
            Paragraph prThree = new Paragraph(context.getString(R.string.cheque_date), boldFont);
            cellThree.addElement(prThree);

            cellThree1.addElement(new Paragraph(context.getString(R.string.colon), boldFont));

            cellFour.setHorizontalAlignment(Element.ALIGN_RIGHT);
            if (!TextUtils.isEmpty(paymentPdfDO.chequeNumber)) {
                if (paymentPdfDO.chequeDate.length() > 0) {
                    String aMonth = paymentPdfDO.chequeDate.substring(4, 6);
                    String ayear = paymentPdfDO.chequeDate.substring(0, 4);
                    String aDate = paymentPdfDO.chequeDate.substring(Math.max(paymentPdfDO.chequeDate.length() - 2, 0));
                    Paragraph prFour = new Paragraph("" + aDate + "/" + aMonth + "/" + ayear, normalFont);
                    cellFour.addElement(prFour);
                }

            } else {
                Paragraph prFour = new Paragraph("", normalFont);
                cellFour.addElement(prFour);
            }


            paymentsTable.addCell(cellOne);
            paymentsTable.addCell(cellOne1);
            paymentsTable.addCell(cellTwo);
            paymentsTable.addCell(cellThree);
            paymentsTable.addCell(cellThree1);
            paymentsTable.addCell(cellFour);

            midleTable.addElement(paymentsTable);
//            document.add(paymentsTable);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addInvoiceNoRowToPdf() {

        try {
            float[] productColumnsWidth = {3, 8};
            PdfPTable paymentsTable = new PdfPTable(productColumnsWidth);
            paymentsTable.setWidthPercentage(100);

            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            cellTwo.setBorder(Rectangle.NO_BORDER);

            cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
            Paragraph prOne = new Paragraph(context.getString(R.string.being_paid), boldFont);
            cellOne.addElement(prOne);

            cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
            String pId = "";
            if (paymentPdfDO.invoiceDos.size() > 1) {
                for (int i = 0; i < paymentPdfDO.invoiceDos.size(); i++) {
                    if (i == paymentPdfDO.invoiceDos.size() - 1) {
                        pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + "";
                    } else {
                        pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + " , ";
                    }

                }

                Phrase phrase = new Phrase("" + pId + " ", normalFont);
                cellTwo.addElement(phrase);
            } else {
                if (paymentPdfDO.invoiceDos.size() > 0 && paymentPdfDO.invoiceDos.get(0).invoiceNumber.length() > 0) {

                    Phrase phrase = new Phrase("" + paymentPdfDO.invoiceDos.get(0).invoiceNumber + " ", normalFont);
                    cellTwo.addElement(phrase);
                }
            }

            paymentsTable.addCell(cellOne);
            paymentsTable.addCell(cellTwo);

//            document.add(paymentsTable);
            midleTable.addElement(paymentsTable);
            //addEmptySpaceLine(1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addRemarksRowToPdf() {
        try {
            PdfPTable noteTable = new PdfPTable(1);
            noteTable.setWidthPercentage(100);

            PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
            String note = "";
            if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                note = podDo.getNotes();
            }
            PdfPCell noteCell = new PdfPCell(new Phrase(context.getString(R.string.remarks) + note, boldFont));
//                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
            noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            noteCell.setPadding(10);
            noteTable.addCell(noteCell);

            document.add(noteTable);
        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

//    private void addSignature() {
//        try {
////
//            PdfPTable custTable = new PdfPTable(1);
//            custTable.setWidthPercentage(100);
//
//            Image image = PDFOperations.getInstance().getSignatureFromFile();
//            if (image != null) {
//                document.add(image);
//            }
//
//            PdfPCell pdfPCell = new PdfPCell(new Phrase("Customer's Signature:", normalFont));
//            pdfPCell.setBorder(Rectangle.NO_BORDER);
//            pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            custTable.addCell(pdfPCell);
//            document.add(custTable);
//
//        } catch (BadElementException e) {
//            e.printStackTrace();
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
//
//    }

    private void addFooterToPdf() {
        try {

            InputStream ims = context.getAssets().open("c_c.png");
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);
            /*Paragraph p = new Paragraph(new Chunk(image, 20, 0, true));
            p.setAlignment(Element.ALIGN_CENTER);
*/
            image.setWidthPercentage(105);
            PdfPCell pdfPCell = new PdfPCell(/*image, true*/);

            pdfPCell.setBorder(Rectangle.NO_BORDER);
            //pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            //          pdfPCell.addElement(p);
            //pdfPCell.setFixedHeight(100f);
            pdfPCell.addElement(image);
            custTable.addCell(pdfPCell);
            document.add(custTable);

            //document.add(PDFOperations.getInstance().addFooterImage(context, document));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendPDfewDoc() {
        if (paymentPdfDO.mail.length() > 0) {
            PDFOperations.getInstance().sendpdfMail(context,
                    paymentPdfDO.mail,
                    paymentPdfDO.customerDescription,
                    PDFConstants.RECEIPT_PDF_NAME, context.getString(R.string.reciept_name));
        } else {
            ((BaseActivity) context).showToast(context.getString(R.string.please_provide_email));
        }


    }

    private PdfPCell getBorderlessCell(String elementName, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(elementName, normalFont));
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }


    private void addEmptySpaceLine(int noOfLines) {
        try {
            PdfPTable emptyTable = new PdfPTable(1);
            emptyTable.setWidthPercentage(100);
            for (int i = 0; i < noOfLines; i++) {
                PdfPCell emptyCell = new PdfPCell();
                emptyCell.setBorder(Rectangle.NO_BORDER);
                emptyCell.addElement(new Paragraph("\n"));
                emptyTable.addCell(emptyCell);
            }
            document.add(emptyTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private String getAddress() {


        String street = paymentPdfDO.customerStreet;
        String landMark = paymentPdfDO.customerLandMark;
        String town = paymentPdfDO.customerTown;
        String postal = paymentPdfDO.customerPostalCode;
        String city = paymentPdfDO.customerCity;
        String countryName = paymentPdfDO.country;

        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
//        if (!TextUtils.isEmpty(countryName)) {
//            finalString += countryName;
//        }

        return finalString;
    }

    public Image getQRCODE() {

        try {

            try {
                Bitmap qrCode = Util.encodeAsBitmap(context, paymentPdfDO.paymentNumber);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                qrCode.compress(Bitmap.CompressFormat.PNG, 100, stream);
                return Image.getInstance(stream.toByteArray());
            } catch (WriterException e) {
                e.printStackTrace();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadElementException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void addQRCode() {
        try {
//

            float[] productColumnsWidth1 = {2, 6};
            PdfPTable sig = new PdfPTable(productColumnsWidth1);
            sig.setWidthPercentage(100);
            Image image = getQRCODE();
            if (image != null) {
                image.setBorder(Rectangle.NO_BORDER);
                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
                pdfPCell.addElement(image);
                sig.addCell(pdfPCell);
                PdfPCell pCell = new PdfPCell();
                pCell.setBorder(Rectangle.NO_BORDER);
                sig.addCell(pCell);
                document.add(sig);

            }

            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);


            PdfPCell pdfPCellSig = new PdfPCell(new Phrase(context.getString(R.string.scan_here), normalFont));
            pdfPCellSig.setBorder(Rectangle.NO_BORDER);
            pdfPCellSig.setHorizontalAlignment(Element.ALIGN_LEFT);
            custTable.addCell(pdfPCellSig);
            document.add(custTable);

            addEmptySpaceLine(1);


        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

}
