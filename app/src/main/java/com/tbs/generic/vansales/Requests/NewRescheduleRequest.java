package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class NewRescheduleRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String shipmentID, date, reason, comment;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private int flaG;

    public NewRescheduleRequest(String shipmentId, int flag, String date, String reason, String comment, Context mContext) {

        this.mContext = mContext;
        this.shipmentID = shipmentId;
        this.flaG = flag;
        this.date = date;
        this.reason = reason;
        this.comment = comment;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO createInvoiceDO, String msg);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String role     = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        int typ     = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);
        String loc = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XDOCTYP", typ);
            jsonObject.put("I_XCREUSR", role);
            jsonObject.put("I_XVEHCODE", loc);

            jsonObject.put("I_XSDHNUM", shipmentID);
            jsonObject.put("I_XRESDATE", date);
            jsonObject.put("I_YREASON", reason);
            if(reason.equalsIgnoreCase("OTH")){
                jsonObject.put("I_YSPECRSON", preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_OTH,""));

            }else {
                jsonObject.put("I_YSPECRSON", "");

            }

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.NEW_CANCEL_RESCHEDULE, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }
String message="";
    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_XSTATUS")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }
                        }
                        if (attribute.equalsIgnoreCase("O_XMESSAGE")) {
                            if (text.length() > 0) {

                                successDO.successFlag = text;
                            }
                        }
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

//        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO,message);
        }
    }
}