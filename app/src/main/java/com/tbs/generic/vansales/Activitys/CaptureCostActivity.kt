package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.tbs.generic.vansales.Adapters.ReasonListAdapter
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ReasonsListRequest
import com.tbs.generic.vansales.Requests.UserorDriverCostTypeRequest
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.activity_capture_cost.*
import kotlinx.android.synthetic.main.activity_dashboard_new.*
import kotlinx.android.synthetic.main.add_capture_cost_dialog.*

class CaptureCostActivity : BaseActivity() {

    lateinit var typelist: Array<String>

    lateinit var paidby: Array<String>


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.activity_capture_cost, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        typelist = arrayOf(resources.getString(R.string.toll),resources.getString(R.string.gas),resources.getString(R.string.meal),resources.getString(R.string.hotel),resources.getString(R.string.repair),resources.getString(R.string.other))
        paidby = arrayOf(resources.getString(R.string.cash),resources.getString(R.string.cheque),resources.getString(R.string.credit),resources.getString(R.string.voucher))

    }

    override fun initializeControls() {
        tvScreenTitle.text = resources.getString(R.string.capture_cost)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        recycleview.layoutManager = LinearLayoutManager(this)
        fab.setOnClickListener {
            var dialog = Dialog(this)
            dialog.setContentView(R.layout.add_capture_cost_dialog)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val window = dialog.window
            if (window != null) {
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            }
            dialog.show()


            val paidbyadapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, paidby)
            paidbyadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            dialog.paid_by.adapter = paidbyadapter
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = UserorDriverCostTypeRequest( 1,this)
                siteListRequest.setOnResultListener { isError, captureTypeMainDO ->
                    hideLoader()
                    if (captureTypeMainDO != null) {
                        if (isError) {
                            showAppCompatAlert("", getString(R.string.please_try_again), getString(R.string.ok), getString(R.string.cancel), "", false)
                        } else {
                            for (i in 0..captureTypeMainDO.reasonDOS.size){
                                typelist.set(i,captureTypeMainDO.reasonDOS[i].typetext)
                            }

                            val typeadapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, typelist)
                            typeadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            dialog.type_spinner.adapter=typeadapter

                        }
                    } else {
                        hideLoader()
                        showAppCompatAlert("", getString(R.string.please_try_again), getString(R.string.ok), getString(R.string.cancel), "", false)

                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

            }


        }


    }
}