package com.tbs.generic.vansales.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;

import androidx.annotation.Nullable;

import java.util.Map;
import java.util.Set;


/**
 * The type Prefs.
 */
public class AppPrefs {

    public static String IS_TIME_CHANGED = "is_time_changed";
    private static SharedPreferences mPrefs;
    private static final String key = "VanselsPref";
    public static final String DRIVER_ID = "DRIVER_ID";
    public static final String STOCK_CONFIRMED = "STOCK_CONFIRMED";
    public static final String CHECK_IN_DONE = "CHECK_IN_DONE";

    public static final String CONFIG_COMPANY_NAME = "config_company_name";
    public static final String CONFIG_IP_ADDRESS = "config_ip_address";
    public static final String CONFIG_PORT = "config_port";
    public static final String CONFIG_PORT_ALIAS = "config_port_alias";
    public static final String CONFIG_USER_NAME = "config_user_name";
    public static final String CONFIG_PASSWORD = "config_password";
    public static final String LANAGUAGE = "language";
    public static final String SERVICE_LANAGUAGE = "service_language";
    public static final String CD_TIME = "CD_TIME";
    public static final String CA_TIME = "CA_TIME";
    public static final String CD_DATE = "CD_DATE";
    public static final String CA_DATE = "CA_DATE";


    public static void initPrefs(Context context) {
        if (mPrefs == null) {
            mPrefs = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        }
    }

    public static void clearallPrefs(Context ctx) {

        getPreferences().edit().clear().apply();

        // / App specific


        // =========

    }

    /**
     * Returns an instance of the shared preference for this app.
     *
     * @return an Instance of the SharedPreference
     */
    public static SharedPreferences getPreferences() {
        if (mPrefs != null) {
            return mPrefs;
        }
        throw new RuntimeException(
                "Prefs class not correctly instantiated please call Prefs.iniPrefs(context) in the Application class onCreate.");
    }

    /**
     * Gets all.
     *
     * @return Returns a map containing a list of pairs key/value representing the preferences.
     * @see SharedPreferences#getAll() SharedPreferences#getAll()SharedPreferences#getAll()SharedPreferences#getAll()SharedPreferences#getAll()
     */
    public static Map<String, ?> getAll() {
        return getPreferences().getAll();
    }

    /**
     * Gets int.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue. Throws ClassCastException if there is a preference with this name that is not an int.
     * @see SharedPreferences#getInt(String, int) SharedPreferences#getInt(String, int)SharedPreferences#getInt(String, int)SharedPreferences#getInt(String, int)SharedPreferences#getInt(String, int)
     */
    public static int getInt(final String key, final int defValue) {
        return getPreferences().getInt(key, defValue);
    }

    /**
     * Gets boolean.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue. Throws ClassCastException if there is a preference with this name that is not a boolean.
     * @see SharedPreferences#getBoolean(String, boolean) SharedPreferences#getBoolean(String, boolean)SharedPreferences#getBoolean(String, boolean)SharedPreferences#getBoolean(String, boolean)SharedPreferences#getBoolean(String, boolean)
     */
    public static boolean getBoolean(final String key, final boolean defValue) {
        return getPreferences().getBoolean(key, defValue);
    }

    /**
     * Gets long.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue. Throws ClassCastException if there is a preference with this name that is not a long.
     * @see SharedPreferences#getLong(String, long) SharedPreferences#getLong(String, long)SharedPreferences#getLong(String, long)SharedPreferences#getLong(String, long)SharedPreferences#getLong(String, long)
     */
    public static long getLong(final String key, final long defValue) {
        return getPreferences().getLong(key, defValue);
    }

    /**
     * Gets float.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue. Throws ClassCastException if there is a preference with this name that is not a float.
     * @see SharedPreferences#getFloat(String, float) SharedPreferences#getFloat(String, float)SharedPreferences#getFloat(String, float)SharedPreferences#getFloat(String, float)SharedPreferences#getFloat(String, float)
     */
    public static float getFloat(final String key, final float defValue) {
        return getPreferences().getFloat(key, defValue);
    }

    /**
     * Gets string.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue. Throws ClassCastException if there is a preference with this name that is not a String.
     * @see SharedPreferences#getString(String, String) SharedPreferences#getString(String, String)SharedPreferences#getString(String, String)SharedPreferences#getString(String, String)SharedPreferences#getString(String, String)
     */
    public static String getString(final String key, @Nullable final String defValue) {
        return getPreferences().getString(key, defValue);
    }

    /**
     * Gets string set.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference values if they exist, or defValues. Throws ClassCastException if there is a preference with this name that is not a Set.
     * @see SharedPreferences#getStringSet(String,
     * Set) SharedPreferences#getStringSet(String,
     * Set)SharedPreferences#getStringSet(String, Set)SharedPreferences#getStringSet(String, Set)SharedPreferences#getStringSet(String, Set)
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static Set<String> getStringSet(final String key,
                                           final Set<String> defValue) {
        SharedPreferences prefs = getPreferences();
        return prefs.getStringSet(key, defValue);
    }

    /**
     * Put long.
     *
     * @param key   The name of the preference to modify.
     * @param value The new value for the preference.
     * @see Editor#putLong(String, long) Editor#putLong(String, long)Editor#putLong(String, long)Editor#putLong(String, long)Editor#putLong(String, long)
     */
    public static void putLong(final String key, final long value) {
        final Editor editor = getPreferences().edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * Put int.
     *
     * @param key   The name of the preference to modify.
     * @param value The new value for the preference.
     * @see Editor#putInt(String, int) Editor#putInt(String, int)Editor#putInt(String, int)Editor#putInt(String, int)Editor#putInt(String, int)
     */
    public static void putInt(final String key, final int value) {
        final Editor editor = getPreferences().edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Put float.
     *
     * @param key   The name of the preference to modify.
     * @param value The new value for the preference.
     * @see Editor#putFloat(String, float) Editor#putFloat(String, float)Editor#putFloat(String, float)Editor#putFloat(String, float)Editor#putFloat(String, float)
     */
    public static void putFloat(final String key, final float value) {
        final Editor editor = getPreferences().edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    /**
     * Put boolean.
     *
     * @param key   The name of the preference to modify.
     * @param value The new value for the preference.
     * @see Editor#putBoolean(String, boolean) Editor#putBoolean(String, boolean)Editor#putBoolean(String, boolean)Editor#putBoolean(String, boolean)Editor#putBoolean(String, boolean)
     */
    public static void putBoolean(final String key, final boolean value) {
        final Editor editor = getPreferences().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * Put string.
     *
     * @param key   The name of the preference to modify.
     * @param value The new value for the preference.
     * @see Editor#putString(String, String) Editor#putString(String, String)Editor#putString(String, String)Editor#putString(String, String)Editor#putString(String, String)
     */
    public static void putString(final String key, final String value) {
        final Editor editor = getPreferences().edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Put string set.
     *
     * @param key   The name of the preference to modify.
     * @param value The new value for the preference.
     * @see Editor#putStringSet(String,
     * Set) Editor#putStringSet(String,
     * Set)Editor#putStringSet(String, Set)Editor#putStringSet(String, Set)Editor#putStringSet(String, Set)
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void putStringSet(final String key, final Set<String> value) {
        final Editor editor = getPreferences().edit();
        editor.putStringSet(key, value);
        editor.apply();
    }

    /**
     * Remove.
     *
     * @param key The name of the preference to remove.
     * @see Editor#remove(String) Editor#remove(String)Editor#remove(String)Editor#remove(String)Editor#remove(String)
     */
    public static void remove(final String key) {
        SharedPreferences prefs = getPreferences();
        final Editor editor = prefs.edit();
        if (prefs.contains(key + "#LENGTH")) {
            // Workaround for pre-HC's lack of StringSets
            int stringSetLength = prefs.getInt(key + "#LENGTH", -1);
            if (stringSetLength >= 0) {
                editor.remove(key + "#LENGTH");
                for (int i = 0; i < stringSetLength; i++) {
                    editor.remove(key + "[" + i + "]");
                }
            }
        }
        editor.remove(key);

        editor.apply();
    }

    /**
     * Contains boolean.
     *
     * @param key The name of the preference to check.
     * @return the boolean
     * @see SharedPreferences#contains(String) SharedPreferences#contains(String)SharedPreferences#contains(String)SharedPreferences#contains(String)SharedPreferences#contains(String)
     */
    public static boolean contains(final String key) {
        return getPreferences().contains(key);
    }

    /**
     * Clear session details
     *
     * @param context the context
     */
    public static void logoutUser(Context context) {
        getPreferences().edit().clear().commit();
        getPreferences().edit().clear().apply();


    }


    /**
     * Clear session details
     *
     * @param context the context
     */
    public static void clearPref(Context context) {
        getPreferences().edit().clear().apply();
    }
}