package com.tbs.generic.vansales.here;

import android.Manifest;

public class Constants {
    public static double Src_Lattitude =3.090582;
    public static double Src_Longitude = 101.745269;
    public static double Des_Lattitude = 17.44477;
    public static double Des_Longitude =78.3763343;

    public static final String[] RUNTIME_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE
    };

    public final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    public static boolean Simulate;
//    public static int  UNDEFINED(0),
//    GO_STRAIGHT(1),
//    UTURN_RIGHT(2),
//    UTURN_LEFT(3),
//    KEEP_RIGHT(4),
//    LIGHT_RIGHT(5),
//    QUITE_RIGHT(6),
//    HEAVY_RIGHT(7),
//    KEEP_MIDDLE(8),
//    KEEP_LEFT(9),
//    LIGHT_LEFT(10),
//    QUITE_LEFT(11),
//    HEAVY_LEFT(12),
//    ENTER_HIGHWAY_RIGHT_LANE(13),
//    ENTER_HIGHWAY_LEFT_LANE(14),
//    LEAVE_HIGHWAY_RIGHT_LANE(15),
//    LEAVE_HIGHWAY_LEFT_LANE(16),
//    HIGHWAY_KEEP_RIGHT(17),
//    HIGHWAY_KEEP_LEFT(18),
//    ROUNDABOUT_1(19),
//    ROUNDABOUT_2(20),
//    ROUNDABOUT_3(21),
//    ROUNDABOUT_4(22),
//    ROUNDABOUT_5(23),
//    ROUNDABOUT_6(24),
//    ROUNDABOUT_7(25),
//    ROUNDABOUT_8(26),
//    ROUNDABOUT_9(27),
//    ROUNDABOUT_10(28),
//    ROUNDABOUT_11(29),
//    ROUNDABOUT_12(30),
//    ROUNDABOUT_1_LH(31),
//    ROUNDABOUT_2_LH(32),
//    ROUNDABOUT_3_LH(33),
//    ROUNDABOUT_4_LH(34),
//    ROUNDABOUT_5_LH(35),
//    ROUNDABOUT_6_LH(36),
//    ROUNDABOUT_7_LH(37),
//    ROUNDABOUT_8_LH(38),
//    ROUNDABOUT_9_LH(39),
//    ROUNDABOUT_10_LH(40),
//    ROUNDABOUT_11_LH(41),
//    ROUNDABOUT_12_LH(42),
//    START(43),
//    END(44),
//    FERRY(45),
//    PASS_STATION(46),
//    HEAD_TO(47),
//    CHANGE_LINE(48);
}
