package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class ValidateDeliveryDO implements Serializable {

    public String shipmentId   = "";
    public int flag            = 0;
    public int flag2           = 0;
    public String message      = "";
    public int status          = 0;
    public int print           = 0;
    public int email           = 0;

}
