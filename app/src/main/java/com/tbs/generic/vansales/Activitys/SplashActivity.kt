package com.tbs.generic.vansales.Activitys


import android.content.Intent
import android.os.Handler
import androidx.drawerlayout.widget.DrawerLayout
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.collector.CollectorCustomerActivity
import com.tbs.generic.vansales.common.FireBaseOperations
import com.tbs.generic.vansales.utils.PreferenceUtils

class SplashActivity : BaseActivity() {

    lateinit var llSplash: RelativeLayout
    override fun initialize() {

        llSplash = layoutInflater.inflate(R.layout.dashboard, null) as RelativeLayout
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        Handler().postDelayed({
            var success = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_SUCCESS, "")



            if (!success.isEmpty()) {
                var role=   preferenceUtils.getIntFromPreference(PreferenceUtils.USER_ROLE, 0)
                if(role==10){
                    intent = Intent(this@SplashActivity, DashBoardActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    intent.putExtra("From", "Login")
                    startActivity(intent)
                    //overridePendingTransition(R.anim.enter, R.anim.exit)
                    finish()
                }
                if(role==20){
                    intent = Intent(this@SplashActivity, DashBoardActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    intent.putExtra("From", "Login")
                    startActivity(intent)
                    //overridePendingTransition(R.anim.enter, R.anim.exit)
                    finish()
                }
                else  if(role==30){
                    intent = Intent(this@SplashActivity, CollectorCustomerActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    intent.putExtra("From", "Login")
                    startActivity(intent)
                    //overridePendingTransition(R.anim.enter, R.anim.exit)
                    finish()
                }else{

                }

            } else {
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "").isEmpty()) {

                    if (preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "").isEmpty()) {
                        intent = Intent(this@SplashActivity, ConfigurationActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                        finish()
                    } else {
                        intent = Intent(this@SplashActivity, LoginActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)
                        finish()
                    }
                }else{
                    intent = Intent(this@SplashActivity, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                }
            }
//            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            startActivity(intent)
//            finish()
        }, 2000)

    }

    override fun initializeControls() {
        toolbar.visibility = View.GONE
        flToolbar.visibility = View.GONE
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
    }

    public override fun onResume() {
        super.onResume()

        FireBaseOperations.checkingDate(this)
    }


}


